from env_dependent import DEVICE, DATA_DIR, EPOCHS


class Config:

    def __init__(self):
        self.DEVICE = DEVICE

        self.BATCH_SIZE = 32
        self.EPOCHS = EPOCHS

        self.MODEL_PATH = f'{DATA_DIR}/checkpoints/resnet50.pth'

        self.TRAIN_SET = f'{DATA_DIR}/datasets/train'
        self.VALIDATION_SET = f'{DATA_DIR}/datasets/validation'
        self.TEST_SET = f'{DATA_DIR}/datasets/test'

        self.PREDICTIONS = f'{DATA_DIR}/submission.csv'