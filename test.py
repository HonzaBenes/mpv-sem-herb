from os import listdir
from os.path import join

import torch
from PIL import Image

from config import Config
from data import image_transforms


def predict(model, test_image_name):
    test_image = Image.open(test_image_name)
    test_image_tensor = image_transforms['test'](test_image)

    if torch.cuda.is_available():
        test_image_tensor = test_image_tensor.view(1, 3, 316, 224).cuda()
    else:
        test_image_tensor = test_image_tensor.view(1, 3, 316, 224)

    with torch.no_grad():
        model.eval()
        # Model outputs log probabilities
        out = model(test_image_tensor)
        # ps = torch.exp(out)
        _, topclass = out.topk(5, dim=1)
        return topclass.flatten().tolist()

if __name__ == '__main__':
    conf = Config()
    model = torch.load(conf.MODEL_PATH, map_location=conf.DEVICE)

    with open(conf.PREDICTIONS, 'w') as f:
        f.write('Id,Category\n')
        for image_name in sorted(listdir(conf.TEST_SET)):
            prediction = predict(model, join(conf.TEST_SET, image_name))
            prediction_str = ' '.join(str(x) for x in prediction)
            print(f'{image_name},{prediction_str}')
            f.write(f'{image_name},{prediction_str}\n')
