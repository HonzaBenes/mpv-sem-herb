from torch import DoubleTensor
from torch.utils.data import DataLoader, WeightedRandomSampler
from torchvision.datasets import ImageFolder
from torchvision.transforms import transforms

from config import Config


def make_weights_for_balanced_classes(images, nclasses):
    count = [0] * nclasses
    for item in images:
        count[item[1]] += 1
    weight_per_class = [0.] * nclasses
    N = float(sum(count))
    for i in range(nclasses):
        weight_per_class[i] = N / float(count[i])
    weight = [0] * len(images)
    for idx, val in enumerate(images):
        weight[idx] = weight_per_class[val[1]]
    return weight


conf = Config()

image_transforms = {
    'train': transforms.Compose([
        transforms.CenterCrop(size=(890, 630)),
        transforms.Resize(size=224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ]),
    'valid': transforms.Compose([
        transforms.CenterCrop(size=(890, 630)),
        transforms.Resize(size=224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ]),
    'test': transforms.Compose([
        transforms.CenterCrop(size=(890, 630)),
        transforms.Resize(size=224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ])
}

train_data = ImageFolder(root=conf.TRAIN_SET, transform=image_transforms['train'])
validation_data = ImageFolder(root=conf.VALIDATION_SET, transform=image_transforms['valid'])
# test_data = ImageFolder(root=conf.TEST_SET, transform=image_transforms['test'])

weights = make_weights_for_balanced_classes(train_data.imgs, len(train_data.classes))
weights = DoubleTensor(weights)
sampler = WeightedRandomSampler(weights, len(weights))

train_loader = DataLoader(train_data, batch_size=conf.BATCH_SIZE, sampler=sampler)
validation_loader = DataLoader(validation_data, batch_size=conf.BATCH_SIZE, shuffle=True)
# test_loader = DataLoader(test_data, batch_size=conf.BATCH_SIZE, shuffle=True)
