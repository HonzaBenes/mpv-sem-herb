import time
from os import listdir
from os.path import isfile

import torch
from torch import nn, optim
from torchvision import models

from config import Config
from data import train_loader

if __name__ == '__main__':
    conf = Config()
    num_class = len(listdir(conf.TRAIN_SET))

    if isfile(conf.MODEL_PATH):
        print("Loading the model checkpoint")
        model = torch.load(conf.MODEL_PATH, map_location=conf.DEVICE)
    else:
        print("Loading pretrained model using torchvision module")
        model = models.resnet50(pretrained=True)

        # for i, param in model.named_parameters():
        #     param.requires_grad = False

        # Replace the last layer
        num_ftrs = model.fc.in_features
        model.fc = nn.Sequential(
            nn.Linear(num_ftrs, num_class),
            nn.LogSoftmax(dim=1)  # For using NLLLoss()
        )

    model = model.to(conf.DEVICE)

    # Define the loss and optimizer
    loss_func = nn.NLLLoss()
    optimizer = optim.Adam(model.parameters())

    for epoch in range(conf.EPOCHS):
        epoch_start = time.time()
        print("Epoch: {}/{}".format(epoch + 1, conf.EPOCHS))

        # Set to training mode
        model.train()

        for i, (inputs, labels) in enumerate(train_loader):
            inputs = inputs.to(conf.DEVICE)
            labels = labels.to(conf.DEVICE)

            # Clean existing gradients
            optimizer.zero_grad()

            # Forward pass - compute outputs on input data using the model
            outputs = model(inputs)

            # Compute loss
            loss = loss_func(outputs, labels)

            # Backpropagate the gradients
            loss.backward()

            # Update the parameters
            optimizer.step()

            # Compute the accuracy
            ret, predictions = torch.max(outputs.data, 1)
            correct_counts = predictions.eq(labels.data.view_as(predictions))

            # Convert correct_counts to float and then compute the mean
            acc = torch.mean(correct_counts.type(torch.FloatTensor))

            print("Batch number: {:03d}, Loss: {:.4f}, Accuracy: {:.4f}".format(i, loss.item(), acc.item()))

    torch.save(model, conf.MODEL_PATH)
