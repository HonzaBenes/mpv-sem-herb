import torch
from torch import nn

from config import Config
from data import validation_loader

if __name__ == '__main__':
    conf = Config()

    model = torch.load(conf.MODEL_PATH, map_location="cpu")

    # Loss and Accuracy within the epoch
    valid_loss = 0.0
    valid_acc = 0.0

    with torch.no_grad():
        # Set to evaluation mode
        model.eval()

        # Validation loop
        for j, (inputs, labels) in enumerate(validation_loader):
            inputs = inputs.to(conf.DEVICE)
            labels = labels.to(conf.DEVICE)

            # Forward pass - compute outputs on input data using the model
            outputs = model(inputs)

            # Compute loss
            # loss = loss_criterion(outputs, labels)
            loss = nn.functional.nll_loss(outputs, labels)

            # Compute the total loss for the batch and add it to valid_loss
            valid_loss += loss.item() * inputs.size(0)

            # Calculate validation accuracy
            ret, predictions = torch.max(outputs.data, 1)
            correct_counts = predictions.eq(labels.data.view_as(predictions))

            # Convert correct_counts to float and then compute the mean
            acc = torch.mean(correct_counts.type(torch.FloatTensor))

            # Compute total accuracy in the whole batch and add to valid_acc
            valid_acc += acc.item() * inputs.size(0)

            print(f"Validation Batch number: {j:03d}, Validation: Loss: {loss.item():.4f}, Accuracy: {acc.item():.4f}")